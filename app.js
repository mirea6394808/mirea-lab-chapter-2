
fetch('database.json')
.then(response => response.json())
.then(data => {
  // Находим студента с наилучшей успеваемостью
  let bestStudent = data.students.reduce((a, b) => {
    let aAvg = a.grades.reduce((acc, val) => acc + val, 0) / a.grades.length;
    let bAvg = b.grades.reduce((acc, val) => acc + val, 0) / b.grades.length;
    return aAvg > bAvg ? a : b;
  });

  // Выводим данные о лучшем студенте на страницу
  document.getElementById('name').textContent = bestStudent.name;
  document.getElementById('subject').textContent = bestStudent.subject;
  document.getElementById('grades').textContent = bestStudent.grades.join(', ');
})
.catch(error => console.error(error));
